/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Supplier;
import javax.tools.JavaFileObject;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class SourceFileReaderTest {

    @Test
    public void 入出力エラー() throws MalformedURLException {
        URL url = new URL("file:Message.java");
        try {
            SourceFileReader.of(url);
        } catch (CompilerException ex) {
            assertThat(ex.getMessage().contains("■■ java code could not read. ■■"), is(true));
        }
    }

    @Test
    public void 文字列コードの読み込みと出力() {
        String code = "class Test {\n"
                      + "}";
        SourceFileReader sourceFileReader = SourceFileReader.of(code);

        Assert.assertEquals(code, sourceFileReader.toSourceCode());
    }

    @Test
    public void コードファイルの読み込みと出力() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        URL url = this.getClass().getClassLoader().getResource("org/vermeerlab/compiler/test/TestClass.java");
        String classString = SourceFileReader.of(url).toSourceCode();
        Class<?> clazz = new ClassCompiler().compile("TestClass", classString);
        Object value = ((Supplier) clazz.getDeclaredConstructor().newInstance()).get();
        Assert.assertThat(value, is("testField"));
    }

    @Test
    public void カバレッジ() {
        SourceFileReader reader = new SourceFileReader();
        URL url = this.getClass().getClassLoader().getResource("org/vermeerlab/compiler/test/TestClass.java");
        JavaFileObject javaFileObject = SourceFileReader.of(url).toJavaFileObject();
    }

}
