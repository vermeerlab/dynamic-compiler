package org.vermeerlab.compiler;

import java.lang.reflect.InvocationTargetException;
import java.util.function.Supplier;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ClassCompilerTest {

    ClassCompiler classCompiler;

    @Before
    public void setUp() throws Exception {
        classCompiler = new ClassCompiler();
    }

    @Test
    public void 文字列からクラスをコンパイル() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        String classString = "public class Test implements java.util.function.Supplier { public Object get(){return \"testclass\";}}";
        Class<?> clazz = new ClassCompiler().compile("Test", classString);
        Object value = ((Supplier) clazz.getDeclaredConstructor().newInstance()).get();
        Assert.assertThat(value, is("testclass"));
    }

    @Test
    public void クラス情報が存在しないためクラスローダーにて異常あり() {
        try {
            this.classCompiler.compile("", "");
        } catch (CompilerException ex) {
            assertThat(ex.getMessage().contains("■■ class loader fail ■■"), is(true));
        }
    }

    @Test
    public void コードが不正のためコンパイルエラーあり() {
        try {
            this.classCompiler.compile("hoge", "fuga");
        } catch (Exception ex) {
            assertThat(ex.getMessage().contains("▼▼ report start ▼▼"), is(true));
        }
    }
}
