/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVisitor;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class SupplyMethodExecutorTest {

    @Test
    public void 動的コンパイルの検証() {
        SupplyMethodInterface supplyMethod = new SupplyMethodInterface() {
            @Override
            public String getMethodBlock() {
                return "return \"test\";";
            }

            @Override
            public TypeMirror getReturnType() {
                TypeMirror typeMirror = new TypeMirror() {
                    @Override
                    public TypeKind getKind() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <R, P> R accept(TypeVisitor<R, P> v, P p) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public List<? extends AnnotationMirror> getAnnotationMirrors() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String toString() {
                        return "java.lang.String";
                    }
                };
                return typeMirror;
            }
        };
        SupplyMethodExecutor executor = SupplyMethodExecutor.of(supplyMethod);
        Object result = executor.toReturnResult();
        Assert.assertThat(result, is("test"));
    }

    @Test
    public void 動的コンパイルエラーの検証() {
        SupplyMethodInterface supplyMethod = new SupplyMethodInterface() {
            @Override
            public String getMethodBlock() {
                return "return \"test\"";
            }

            @Override
            public TypeMirror getReturnType() {
                TypeMirror typeMirror = new TypeMirror() {
                    @Override
                    public TypeKind getKind() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <R, P> R accept(TypeVisitor<R, P> v, P p) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public List<? extends AnnotationMirror> getAnnotationMirrors() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String toString() {
                        return "java.lang.String";
                    }
                };
                return typeMirror;
            }
        };
        SupplyMethodExecutor executor = SupplyMethodExecutor.of(supplyMethod);
        try {
            Object result = executor.toReturnResult();
        } catch (Exception ex) {
            Assert.assertThat(ex.toString().contains("report start"), is(true));
        }

    }

    @Test
    public void コンストラクタカバレッジ() {
        SupplyMethodExecutor supplyMethodExecutor = new SupplyMethodExecutor();
    }

    @Test(expected = CompilerException.class)
    public void 例外ルートカバレッジ() {
        SupplyMethodInterface supplyMethod = new SupplyMethodInterface() {
            @Override
            public String getMethodBlock() {
                return "return \"test\"";
            }

            @Override
            public TypeMirror getReturnType() {
                TypeMirror typeMirror = new TypeMirror() {
                    @Override
                    public TypeKind getKind() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <R, P> R accept(TypeVisitor<R, P> v, P p) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public List<? extends AnnotationMirror> getAnnotationMirrors() {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String toString() {
                        throw new IllegalArgumentException();
                    }
                };
                return typeMirror;
            }
        };
        SupplyMethodExecutor executor = SupplyMethodExecutor.of(supplyMethod);
        Object result = executor.toReturnResult();
    }
}
