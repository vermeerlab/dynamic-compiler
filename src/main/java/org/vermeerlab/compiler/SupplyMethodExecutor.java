/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.lang.reflect.InvocationTargetException;
import java.util.function.Supplier;

/**
 * メソッド内部実装をオンメモリで実行するための機能を提供します.
 * <P>
 * メソッドの内部実装を{@link java.util.function.Supplier} に置き換えて動的にオンメモリでコンパイルして実行するため、
 * 引数なし・戻り値あり のメソッドが対象です.
 *
 * @see SupplyMethodInterface
 * @author Yamashita,Takahiro
 */
public class SupplyMethodExecutor {

    private final static String TEMP_CLASSNAME = "TespClass";

    final SupplyMethodInterface supplyMethod;

    /**
     * for expansion
     */
    protected SupplyMethodExecutor() {
        this.supplyMethod = null;
    }

    protected SupplyMethodExecutor(SupplyMethodInterface supplyMethod) {
        this.supplyMethod = supplyMethod;
    }

    /**
     * インスタンスを構築します.
     *
     * @param supplyMethod 動的コンパイルをしたいメソッド情報
     * @return 構築したインスタンス
     */
    public static SupplyMethodExecutor of(SupplyMethodInterface supplyMethod) {
        return new SupplyMethodExecutor(supplyMethod);
    }

    /**
     * メソッドの内部実装をコンパイルして実行した結果を返却します.
     * <P>
     * 型は汎用的な {@code Object} なので呼出元でキャストをしてください.
     *
     * @return 参照元メソッドの戻り値
     * @throws CompilerException コードコンパイル時にエラーがあった場合
     */
    public Object toReturnResult() {
        try {
            String javaCode = this.toJavaCode();
            Class<?> clazz = new ClassCompiler().compile(TEMP_CLASSNAME, javaCode);
            return ((Supplier) clazz.getDeclaredConstructor().newInstance()).get();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException ex) {
            throw new CompilerException(ex, "On Memmory Compile Supplier #get can not access. ");
        }
    }

    /**
     * メソッドの文字列をコンパイルできるように {@link java.util.function.Supplier<T>} を実装した関数型クラスに編集します.
     *
     * @return メソッド内部実装を単体のJavaCodeに編集した文字列
     */
    String toJavaCode() {
        String _ReturnType = this.supplyMethod.getReturnType().toString();

        StringBuilder code = new StringBuilder();
        code.append("import ");
        code.append(_ReturnType);
        code.append(";");
        code.append("public class ");
        code.append(TEMP_CLASSNAME);
        code.append(" implements java.util.function.Supplier<");
        code.append(_ReturnType);
        code.append(">{");
        code.append(" public ");
        code.append(_ReturnType);
        code.append(" get(){ ");
        code.append(supplyMethod.getMethodBlock());
        code.append(" }");
        code.append(" }");

        return code.toString();
    }
}
