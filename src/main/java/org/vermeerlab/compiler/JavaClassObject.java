/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;

/**
 * コンパイルされたクラス（バイトコード）を保持するファイルオブジェクトです.
 *
 * @author Yamashita,Takahiro
 */
public class JavaClassObject extends SimpleJavaFileObject {

    final ByteArrayOutputStream bos;
    Class<?> clazz = null;

    /**
     * インスタンスを構築します.
     * <P>
     * {@inheritDoc}
     */
    public JavaClassObject(String name, Kind kind) {
        super(URI.create("string:///" + name.replace('.', '/') + kind.extension), kind);
        bos = new ByteArrayOutputStream();
    }

    /**
     * 出力用ストリームを返却します.
     * <P>
     * {@inheritDoc}
     */
    @Override
    public OutputStream openOutputStream() throws IOException {
        return this.bos;
    }

    /**
     * コンパイルされたバイトコードを返却します.
     *
     * @return バイトコード
     */
    public byte[] getBytes() {
        return this.bos.toByteArray();
    }

    /**
     * ロードされたクラスを設定します.
     *
     * @param clazz ロードしたクラス
     */
    public void setDefinedClass(Class<?> clazz) {
        this.clazz = clazz;
    }

    /**
     * ロードされたクラスを返却します.
     *
     * @return ロードしたクラス
     */
    public Class<?> getDefinedClass() {
        return this.clazz;
    }
}
