/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.io.IOException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.SecureClassLoader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.tools.DiagnosticListener;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;

/**
 * オンメモリーコンパイルが出来るように出力用ファイルの生成部分をファイルオブジェクトにして管理します.
 *
 * @author Yamashita,Takahiro
 */
public class ClassFileManager extends ForwardingJavaFileManager<JavaFileManager> {

    /**
     * キー：クラス名、値：クラスファイルのオブジェクト
     */
    final Map<String, JavaClassObject> map;

    /**
     * インスタンスを構築します.
     *
     * @param compiler 実行環境から取得したCompilerオブジェクト
     * @param listener コンパイル結果を出力するリスナー
     */
    public ClassFileManager(JavaCompiler compiler, DiagnosticListener<? super JavaFileObject> listener) {
        super(compiler.getStandardFileManager(listener, null, null));
        this.map = new ConcurrentHashMap<>();
    }

    /**
     * 指定された場所にある、特定の種類の特定のクラスを表す出力用ファイルオブジェクトを返却します.
     * <P>
     * @param location 場所
     * @param className クラスの名前
     * @param kind ファイルの種類。SOURCE または CLASS のいずれかである必要がある
     * @param sibling - 配置のヒントとして使用されるファイルオブジェクト。次も可: null
     * @return 出力用ファイルオブジェクト
     *
     * @throws IllegalArgumentException 入出力エラーが発生した場合、または JavaFileManager.close() が呼び出され、このファイルマネージャーを再度開くことができない場合
     * @throws IllegalStateException JavaFileManager.close() が呼び出され、このファイルマネージャーを再度開くことができない場合
     * @throws IOException 入出力エラーが発生した場合、または JavaFileManager.close() が呼び出され、このファイルマネージャーを再度開くことができない場合
     */
    @Override
    public JavaFileObject getJavaFileForOutput(Location location, String className, Kind kind, FileObject sibling) throws IOException {
        JavaClassObject javaClassObject = new JavaClassObject(className, kind);
        this.map.put(className, javaClassObject);
        return javaClassObject;
    }

    /**
     * コンパイルしたクラスを返す為のクラスローダー
     */
    ClassLoader loader = null;

    /**
     * コンパイルしたクラスを返す為のクラスローダーを返却します.
     * <P>
     * {@inheritDoc }
     */
    @Override
    public ClassLoader getClassLoader(Location location) {
        if (loader == null) {
            loader = AccessController.doPrivileged((PrivilegedAction<Loader>) () -> {
                return new Loader(ClassFileManager.class.getClassLoader());
            });
        }
        return loader;
    }

    /**
     * コンパイルしたクラスを返す為のクラスローダー
     */
    class Loader extends SecureClassLoader {

        protected Loader(ClassLoader classLoader) {
            super(classLoader);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            JavaClassObject co = map.get(name);
            if (co == null) {
                throw new ClassNotFoundException(name);
            }

            Class<?> c = co.getDefinedClass();
            if (c == null) {
                byte[] b = co.getBytes();
                c = super.defineClass(name, b, 0, b.length);
                co.setDefinedClass(c);
            }
            return c;
        }
    }
}
