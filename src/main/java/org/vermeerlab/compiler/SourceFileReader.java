/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;

/**
 * リソースからJavaFileObjectを読み込む機能を提供します.
 * <P>
 * ソースコードの定数にUTF-8のマルチバイトが含まれる場合、文字化けしないように
 * 本クラスではUTF-8を考慮した読み込みを行い、{@literal JavaFileObject}を生成します.
 *
 * @author Yamashita,Takahiro
 */
public class SourceFileReader {

    final String sourceCode;

    /**
     * for expansion
     */
    protected SourceFileReader() {
        this.sourceCode = null;
    }

    protected SourceFileReader(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    /**
     * URLからソースファイルを取得し {@literal JavaFileObject} を作成するためのインスタンスを構築します.
     * <P>
     * ソースファイルはUTF-8でフォーマットされていることを前提としています.
     *
     * @param url 参照元のJavaファイルのURL
     * @return 構築したインスタンス
     */
    public static SourceFileReader of(URL url) {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            StringBuilder sb = new StringBuilder();
            in.lines().forEachOrdered(line -> {
                sb.append(line);
                sb.append("\n");
            });
            return new SourceFileReader(sb.toString());
        } catch (IOException ex) {
            throw new CompilerException(ex, "■■ java code could not read. ■■");
        }
    }

    /**
     * 文字列から {@literal JavaFileObject} を作成するためのインスタンスを構築します.
     * <P>
     * @param sourceCode ソースコード文字列
     * @return 構築したインスタンス
     */
    public static SourceFileReader of(String sourceCode) {
        return new SourceFileReader(sourceCode);
    }

    /**
     * 文字列ソースコードから {@literal JavaFileObject} を作成します.
     * <P>
     * 一時的に使用するため、クラス名が不要な場合に使用してください.
     *
     * @return 文字列から生成したJavaFIleObject
     */
    public JavaFileObject toJavaFileObject() {
        return new SourceFileReader.StringJavaFileObject("", this.sourceCode);
    }

    /**
     * 文字列ソースコードからJavaFileObjectを作成します.
     * <P>
     * 作成するクラスに名前をつけたい場合に使用してください.
     *
     * @param className 生成するクラスの完全修飾名
     * @return 文字列から生成したJavaFIleObject
     */
    public JavaFileObject toJavaFileObject(String className) {
        return new SourceFileReader.StringJavaFileObject(className, this.sourceCode);
    }

    /**
     * Javaファイルから取得したコードを文字列にして返却します.
     *
     * @return ソースコード
     */
    public String toSourceCode() {
        return this.sourceCode;
    }

    /**
     * 文字列からJavaFileObjectを作成します.
     */
    private static class StringJavaFileObject extends SimpleJavaFileObject {

        final String content;

        /**
         * @param className クラスの完全修飾名
         * @param source 生成JavaFileObjectのコード文字列
         */
        public StringJavaFileObject(String className, String source) {
            super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.content = source;
        }

        /**
         * {@inheritDoc }
         */
        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            return this.content;
        }
    }

}
