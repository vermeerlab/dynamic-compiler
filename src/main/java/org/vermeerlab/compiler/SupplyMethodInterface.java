/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import javax.lang.model.type.TypeMirror;

/**
 * メソッドの実装をオンメモリでコンパイルして実行するためのインターフェースです.
 * <P>
 * 引数なし 戻り値ありのメソッドの内部実装を 動的に{@link java.util.function.Supplier} で実装したクラスをオンメモリでコンパイルして実行するために使用します.
 * <P>
 * {@link #getMethodBlock() }で返却した文字列を元に動的コンパイルの情報として使用したり内部実装検証することを想定しています.
 * メソッドが空の場合は {@code null} ではなく、空文字を返却します.<br>
 * 文字列情報を元に動的コンパイルをする場合、単純名ではクラスの特定が出来ないためコンパイルエラーになる可能性があります.
 * その場合は コードブロック内のクラスは単純名ではなく完全修飾名で実装をしてください.
 *
 * @see SupplyMethodExecutor
 * @author Yamashita,Takahiro
 */
public interface SupplyMethodInterface {

    /**
     * 内部実装の文字列を返却します.
     *
     * @return 要素の内部情報.
     */
    public String getMethodBlock();

    /**
     * 戻り値の型を返却します.
     * <P>
     * @return 要素のメソッドの型.
     */
    public TypeMirror getReturnType();
}
