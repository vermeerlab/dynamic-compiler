/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import java.util.Arrays;
import java.util.List;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import org.vermeerlab.scanner.UniqueClasspathElements;

/**
 * コード文字列を元にコンパイルをする機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
public class ClassCompiler {

    final DiagnosticListener<? super JavaFileObject> listener;
    final JavaCompiler compiler;

    /**
     * インスタンスを構築します.
     */
    public ClassCompiler() {
        this.listener = new ErrorListener();
        this.compiler = ToolProvider.getSystemJavaCompiler();
    }

    /**
     * コード文字列からコンパイルをします.
     *
     * @param className コンパイルして生成するクラスの完全修飾名
     * @param sourceCode コンパイルするコード文字列
     * @return コード文字列からコンパイルしたクラス
     * @throws CompilerException コードコンパイル時にエラーがあった場合
     */
    public Class<?> compile(String className, String sourceCode) {
        JavaFileObject javaFileObject = SourceFileReader.of(sourceCode).toJavaFileObject(className);

        List<JavaFileObject> compilationUnits = Arrays.asList(javaFileObject);

        String classpaths = UniqueClasspathElements.of().getPathsString();

        List<String> options = Arrays.asList("-classpath", classpaths);
        JavaFileManager manager = new ClassFileManager(compiler, listener);
        CompilationTask task = compiler.getTask(
                null,
                manager, //出力ファイルを扱うマネージャー
                listener, //エラー時の処理を行うリスナー（nullでもよい）
                options, //コンパイルオプション
                null,
                compilationUnits //コンパイル対象ファイル群
        );

        //コンパイルを実行
        task.call();

        ClassLoader classLoader = manager.getClassLoader(null);
        try {
            @SuppressWarnings("unchecked")
            Class<?> clazz = classLoader.loadClass(className);
            return clazz;
        } catch (ClassNotFoundException ex) {
            throw new CompilerException(ex, "■■ class loader fail ■■", className);
        }
    }
}
