/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import org.vermeerlab.base.AbstractCustomException;

/**
 * Compiler処理中に発生した実行時例外です.
 * <P>
 * 例外発生個所を明確にすることを目的とした{@link org.vermeerlab.base.AbstractCustomException}のラッパーであり独自の実装はしていません.
 *
 * @author Yamashita,Takahiro
 */
public class CompilerException extends AbstractCustomException {

    private static final long serialVersionUID = 1L;

    /**
     * 実行時例外を構築します.
     */
    public CompilerException() {
    }

    /**
     * 実行時例外を構築します.
     *
     * @param messages 例外情報に付与する文字列
     */
    public CompilerException(String... messages) {
        super(messages);
    }

    /**
     * メッセージに加えて実行時例外情報のスタックトレース情報を付与した実行時例外を構築します.
     * <P>
     * @param ex 例外
     * @param messages 例外情報に付与する文字列
     */
    public CompilerException(Exception ex, String... messages) {
        super(ex, messages);
    }
}
