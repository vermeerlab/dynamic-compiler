/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.compiler;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;

/**
 * コンパイルエラーの結果を格納するリスナーです.
 *
 * @author Yamashita,Takahiro
 */
public class ErrorListener implements DiagnosticListener<JavaFileObject> {

    /**
     * インスタンスを構築します.
     */
    public ErrorListener() {
    }

    /**
     * {@inheritDoc }
     * <P>
     * @throws CompilerException エラーレポートを出力する場合
     */
    @Override
    public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
        throw new CompilerException(
                String.join(System.lineSeparator(),
                            "▼▼ report start ▼▼",
                            "errcode：" + diagnostic.getCode(),
                            "line   ：" + diagnostic.getLineNumber(),
                            "column ：" + diagnostic.getColumnNumber(),
                            "message：" + diagnostic.getMessage(null),
                            diagnostic.toString(),
                            "▲▲ report end ▲▲"
                )
        );
    }
}
