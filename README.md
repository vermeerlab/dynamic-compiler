動的コンパイラー
===

文字列からクラスをコンパイルする機能を提供します.

## Description


## Requirement
事前にローカルリポジトリに `parentpom` プロジェクトを作成してください。

`git clone https://bitbucket.org/vermeerlab/parentpom.git`

## Usage
Maven Repository

https://github.com/vermeerlab/maven/tree/mvn-repo/org/vermeerlab/vermeerlab-dynamic-compiler

## Licence

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author

[_vermeer_](https://twitter.com/_vermeer_)